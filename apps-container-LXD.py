import subprocess


def initialize():
    # Initialize LXD, a YAML configuration on standard input.
    # noinspection SpellCheckingInspection
    process = subprocess.Popen(["lxd", "init", "--preseed"], stdin=subprocess.PIPE, universal_newlines=True)
    process.communicate(input="""\
config: {}
networks: []
storage_pools: []
profiles:
- config: {}
  description: Default LXD profile
  devices: {}
  name: default
projects:
- config:
    features.images: "true"
    features.networks: "true"
    features.profiles: "true"
    features.storage.buckets: "true"
    features.storage.volumes: "true"
  description: Default LXD project
  name: default\n""")


if __name__ == "__main__":
    from pylxd import Client
    import pylxd.exceptions

    try:
        initialize()
        client = Client()
        # ...
    except pylxd.exceptions.ClientConnectionFailed:
        print("Failed to connect to the LXD server on this computer.")
        print(" \"sudo\" is needed when running this file.")
