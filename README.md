# Apps sorter and container

This is _going to be_ a (Windows/Linux) tool, to do two (2) jobs:

1. _Apps sorter_ - to sort, find, list all installed apps
2. _Organized Linux apps container_ - to use Linux application(s), contained inside:
   * (**on Linux systems**) LXD _system container_ 
   * (**on Windows**) WSL - Windows Subsystem for Linux _lightweight virtual machine_

System [containers](https://www.freecodecamp.org/news/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b/) make software able to be

> moved and run consistently in any environment and on any infrastructure,  
> independent of that environment or infrastructure’s operating system

([Red Hat](https://www.redhat.com/en/topics/cloud-native-apps/what-is-containerization#:~:text=moved%20and%20run%20consistently%20in%20any%20environment%20and%20on%20any%20infrastructure%2C%20independent%20of%20that%20environment%20or%20infrastructure%E2%80%99s%20operating%20system)).

## Apps sorter

* "Favo(u)rite"
* "Recent (used lately)"
* "All applications..." (in categories)
* https://pypi.org/project/winapps/ - winapps · PyPI - Python library for managing installed applications on Windows
* https://askubuntu.com/questions/433609/how-can-i-list-all-applications-installed-in-my-system/435082#435082 - "python list installed apps Unix"

Python framework for the application GUI - [Kivy](https://kivy.org/doc/stable/gettingstarted/intro.html) (_cross-platform_).

## Organized Linux apps container

### On Linux

Install [**Python 3**](https://www.python.org/downloads/), 
[**LXD**](https://documentation.ubuntu.com/lxd/en/latest/installing/#install-lxd-from-a-package) 
and run these commands (`sudo` is needed, to access the LXD daemon):

```
sudo python3 -m ensurepip --default-pip   # to install pip package installer
sudo python3 -m pip install --upgrade pip # update pip
sudo python3 -m pip install pylxd         # pylxd library
```

-- used [Installing Packages — Python Packaging User Guide](https://packaging.python.org/en/latest/tutorials/installing-packages/#ensure-you-can-run-pip-from-the-command-line),
[/doc/source/installation.rst (canonical/pylxd)](https://github.com/canonical/pylxd/blob/43f5eb1ae85d0d81de1fbc3651799923c1fc8a58/doc/source/installation.rst)

**VNC**? (container)\
**X window manager** - [Awesome, awesomewm.org](https://awesomewm.org/apidoc/documentation/07-my-first-awesome.md.html)

_https://github.com/prompt-toolkit/python-prompt-toolkit_

**https://github.com/MicrosoftDocs/wsl/blob/main/WSL/install.md#prerequisites ,
https://learn.microsoft.com/en-us/windows/wsl/filesystems#interoperability-between-windows-and-linux-commands**

~~install it...~~------------------------

1. `git clone https://gitlab.com/logical-E/linux-container-lxd-on-usb.git`
2. _  
   ```bash
   # cat Pre-seed_YAML_config | sudo lxd init --preseed
   #
   #
   
   sudo lxc storage create pool1 zfs source="$(read -p 'Path to the USB block device, partition 2: '; echo $REPLY)"

   lxc image list images: mint  # - for Linux Mint, version **** (mint/vanessa ?)

   sudo lxc launch images:mint/**** linux-mint-container --storage pool1

   ```
### User(s) / app config file storage

```bash
sudo lxc storage volume create pool1 config-home-dir--volume  # custom storage volume: system-wide configuration files, users' home directories

sudo lxc storage volume attach pool1 config-home-dir--volume linux-mint-container /custom-storage

sudo lxc exec linux-mint-container -- /bin/bash -c 'mv -t /custom-storage/ /etc/ /home/ && ln --symbolic -t / /custom-storage/etc/ /custom-storage/home/'


user='**USER TO ADD**'; sudo lxc exec linux-mint-container -- /bin/bash -c "adduser ${user}; chown -R ${user}:sudo /custom-storage/home/${user}; usermod -aG sudo ${user};"
```

### Networks, SSH

```bash
sudo lxc stop linux-mint-container

sudo lxc network create net --type=bridge dns.domain=1.1.1.1
sudo lxc network list

sudo lxc network attach net linux-mint-container net eth0
sudo lxc config device show linux-mint-container              # Show full device configuration

sudo lxc start linux-mint-container


sudo lxc exec linux-mint-container -- /bin/bash

ip address
apt-get install openssh-server xauth nano && nano /etc/ssh/sshd_config   # "X11Forwarding yes" must be specified

exit

sudo lxc config device add linux-mint-container login proxy listen=tcp:127.0.0.1:32822 connect=tcp:127.0.0.1:22

sudo apt-get install openssh-client

ssh-keygen -f ~/.ssh/linux-mint-container

ssh-copy-id -i ~/.ssh/linux-mint-container.pub -p 32822 ${user}@127.0.0.1
```

-- https://stackoverflow.com/questions/13210880/replace-one-substring-for-another-string-in-shell-script
-- SSH, forward X over it - https://unix.stackexchange.com/a/12772/491028

---

Access files: [Linux Containers - LXD - Getting started](https://linuxcontainers.org/lxd/getting-started-cli/#access-files), `sudo lxc file mount --help`  
User login: `source ./_source__container_login_func.sh`  
Stop: `sudo lxc stop linux-mint-container; sudo lxd shutdown;`

## License

It is subject to the terms of the Mozilla Public License, v2.0.
